module.exports = {
  extends: 'jameda',
  rules: {
    'import/no-extraneous-dependencies': 'off'
  },
  settings: {
    'import/resolver': {
      node: {
        moduleDirectory: ['node_modules', 'app']
      }
    }
  }
};
