module.exports = {
  extends: ['jameda', 'plugin:jest/recommended'],
  plugins: ['jest'],
  env: {
    jest: true
  },
  settings: {
    'import/resolver': {
      node: {
        moduleDirectory: ['node_modules', 'app', 'test']
      }
    }
  },
  rules: {
    'global-require': 'off'
  }
};
