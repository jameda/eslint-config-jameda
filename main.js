module.exports = {
  parser: 'babel-eslint',
  extends: ['airbnb', 'prettier', 'prettier/react'],
  plugins: ['react'],
  env: {
    es6: true,
    browser: true
  },
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
    ecmaFeatures: {
      defaultParams: true,
      classes: true,
      jsx: true
    }
  },
  settings: {
    'import/resolver': {
      node: {
        moduleDirectory: ['node_modules', 'app']
      }
    }
  },
  rules: {
    'comma-dangle': ['error', 'never'],
    'linebreak-style': 'off',
    'no-else-return': 'off',
    'no-warning-comments': 'warn',
    'prefer-destructuring': ['error', { object: true, array: false }],
    'lines-between-class-members': ['error', 'always'],

    'prefer-destructuring': [
      'error',
      {
        VariableDeclarator: {
          array: false,
          object: true
        },
        AssignmentExpression: {
          array: false,
          object: false
        }
      }
    ],

    quotes: ['error', 'single'],

    'jsx-a11y/anchor-is-valid': ['warn', { aspects: ['invalidHref'] }],

    // needed for babel-plugin-transform-react-remove-prop-types
    'react/forbid-foreign-prop-types': 'error',

    // allow any file extension
    'react/jsx-filename-extension': 'off',

    // enables custom Shapes in PropTypes
    'react/no-typos': 'off',

    // prefer named exports
    'import/prefer-default-export': 'off'
  }
};
