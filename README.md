# eslint-config-jameda

Shareable eslint config for Jameda.

Sadly,
[it's not possible to bundle plugins yet as an eslint shareable config](https://github.com/eslint/eslint/issues/3458).

For this reason, to install and use this config you will need to install all
plugin peerDepencencies:

```sh
npm install --save-dev babel-eslint@8.0.3
npm install --save-dev eslint@4.12.1
npm install --save-dev eslint-config-airbnb@16.1.0
npm install --save-dev eslint-plugin-import@2.8.0
npm install --save-dev eslint-plugin-jest@21.4.1
npm install --save-dev eslint-plugin-jsx-a11y@6.0.2
npm install --save-dev eslint-plugin-react@7.5.1
```

and then add an `.eslintrc` file to your js project with the following content:

```js
{
  "extends": "jameda"
}
```

For `test` directory, use `jameda/test` instead. This will allow for relative
imports inside test files:

```js
// not so nice
import MyComponent from '../../../../app/components/MyComponent';

// nicer, but you need to tell eslint how to resolve this path
import MyComponent from 'components/MyComponent';
```
